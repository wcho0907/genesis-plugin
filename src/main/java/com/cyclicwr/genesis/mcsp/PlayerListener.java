package com.cyclicwr.genesis.mcsp;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {
    private final GenesisPlugin plugin;

    public PlayerListener(GenesisPlugin instance) {
        plugin = instance;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        plugin.getLogger().info(event.getPlayer().getName() + " joined the server! :D");
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        plugin.getLogger().info(event.getPlayer().getName() + " left the server! :'(");
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if (plugin.isDebugging(event.getPlayer())) {
            Location from = event.getFrom();
            Location to = event.getTo();

            plugin.getLogger().info(String.format("From %.2f,%.2f,%.2f to %.2f,%.2f,%.2f", from.getX(), from.getY(), from.getZ(), to.getX(), to.getY(), to.getZ()));
        }
        
        Player p = event.getPlayer();
        
        // Get the Block right below the player
        Block b = p.getLocation().getBlock().getRelative(BlockFace.DOWN);
        
        // Create an explosion of power 5 on the player's location
        if (b.getType() == Material.DARK_OAK_LOG){
        
            World w = p.getWorld();
            w.createExplosion(p.getLocation(), 5);
        
        }
        
        Player player = p;
        if (player.getItemInHand().getType() == Material.FISHING_ROD) {
            // Creates a bolt of lightning at a given location. In this case, that location is where the player is looking.
            // Can only create lightning up to 200 blocks away.
            player.getWorld().strikeLightning(player.getTargetBlock((Set<Material>) null, 200).getLocation());
        }
        
        Block block = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
        

    	if(block.getType() == Material.DIRT) {
    		block.setType(Material.DIAMOND_ORE);
    	}

    }
    
    @EventHandler
    public void onMobSpawn(CreatureSpawnEvent event){

        LivingEntity entity = event.getEntity();

        if(entity instanceof Monster){

            // Give the Monster full diamond armor.
            entity.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
            entity.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
            entity.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
            entity.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
            if(entity.getType() == EntityType.SKELETON){

                // Create an enchanted bow and give it to the skeleton.
                ItemStack bow = new ItemStack(Material.BOW);
                bow.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
                entity.getEquipment().setItemInMainHand(bow);

            }

//            if (entity.getType() == EntityType.CREEPER){
//
//                entity.setInvisible(true);
//
//            }


        }
    }
}